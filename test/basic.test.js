const Server = require('../')
const Client = require('ahau-graphql-client')

const test = require('tape')
const Testbot = require('scuttle-testbot')
const fetch = require('node-fetch')
const { promisify } = require('util')

test('basic', async t => {
  const stack = Testbot
    .use(require('ssb-backlinks'))
    .use(require('ssb-query'))
    .use(require('ssb-profile'))
    .use(require('ssb-settings'))
    .use(require('ssb-blobs'))
    .use(require('ssb-tribes'))
    .use(require('ssb-recps-guard'))

  const ssb = stack({
    db1: true,
    hyperBlobs: { port: 7777 }
  })

  const port = 1000 + Math.random() * 10000 | 0

  const main = require('@ssb-graphql/main')(ssb)
  const profile = require('@ssb-graphql/profile')(ssb)
  const context = await promisify(main.loadContext)()

  const httpServer = await Server({
    port,
    schemas: [main, profile],
    context
  })
  t.pass(`http://localhost:${port}/graphql`)

  const client = new Client(`http://localhost:${port}/graphql`, { isTesting: true, fetch })
  // NOTE fetch is only needed in testing (as not in the browser)

  const result = await client.query({
    query: `{
      whoami {
        public {
          feedId
          profileId
        }
        personal {
          profileId
        }
      }
    }`
  })
    .catch(errors => ({ errors }))

  // console.log(JSON.stringify(result, null, 2))

  t.equal(result.data.whoami.public.feedId, ssb.id, 'whoami.public.feedId')
  t.equal(result.data.whoami.public.profileId.slice(0, 1), '%', 'whoami.public.profilId')
  t.equal(result.data.whoami.personal.profileId.slice(0, 1), '%', 'whoami.personal.profilId')

  httpServer.close()
  ssb.close()
  t.end()
})
