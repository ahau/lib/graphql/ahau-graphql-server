const http = require('http')
const express = require('express')
const cors = require('cors')
const { promisify } = require('util')
const { createYoga } = require('graphql-yoga')
const { graphqlUploadExpress } = require('graphql-upload')
const { buildSubgraphSchema } = require('@apollo/subgraph')

module.exports = function graphqlServer ({ context, schemas, port, allowedOrigins }, cb) {
  if (cb === undefined) return promisify(graphqlServer)({ context, schemas, port, allowedOrigins })

  if (allowedOrigins && !Array.isArray(allowedOrigins)) {
    return cb(new Error(`expected Array of allowedOrigins, got ${allowedOrigins}`))
  }

  const app = express()

  const graphiqlOrigin = `http://localhost:${port}`
  const devServerOrigin = `http://localhost:${process.env.DEV_SERVER_PORT || 3000}`

  app.use(cors({
    origin (origin, cb) {
      if (allowedOrigins && allowedOrigins.some(ao => ao === origin)) {
        return cb(null, true)
      }

      if (process.env.PLATFORM === 'cordova') {
        if (origin === undefined) return cb(null, true)
        if (origin === 'file://') return cb(null, true)
      }

      switch (process.env.NODE_ENV) {
        case undefined: // if NODE_ENV not set, apply production standards
        case 'production':
          if (origin === undefined) return cb(null, true) // Electron
          if (origin === graphiqlOrigin) return cb(null, true) // GraphiQL
          return cb(new Error('CORS issue')) // give no hints

        case 'development':
        case 'test':
          if (origin === undefined) return cb(null, true) // Electron
          if (origin === graphiqlOrigin) return cb(null, true) // GraphiQL
          if (origin === devServerOrigin) return cb(null, true) // Webpack dev server
          return cb(new Error(`CORS issue - unexpected header 'Origin': '${origin}'`))

        default:
          cb(new Error(`invalid NODE_ENV - ${process.env.NODE_ENV}`))
      }
    }
  }))

  const MAX_FILE_SIZE = 5 * 1024 * 1024 // 5MB

  app.use(
    '/graphql',
    graphqlUploadExpress({ maxFileSize: MAX_FILE_SIZE, maxFiles: 10 }),
    createYoga({
      schema: buildSubgraphSchema(schemas),
      context: (req) => context,
      // Context factory gets called for every request
      graphiql: true
      // maskedErrors: false
      // NOTE this leaks some info about errors, but given we're working locally with graphql
      // I think this is ok.
      // See https://the-guild.dev/graphql/yoga-server/docs/features/error-masking#disabling-error-masking
    })
  )

  const httpServer = http.createServer(app)

  httpServer.listen(port, (err) => {
    if (err) return cb(err)

    /*
      cordova-bridge is used in Cordova, DO NOT REMOVE!
      The if statement is uncommented by patch-package in the mobile platform
    */
    // if (process.env.PLATFORM === 'cordova') {
    //   require('cordova-bridge').channel.post(
    //     'initialized',
    //     JSON.stringify({ started: true })
    //   )
    // }

    cb(null, httpServer)
  })
}
