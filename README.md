# ahau-graphql-server

## Example Usage

```js
const ahauServer = require('ahau-graphql-server')

const main = require('@ssb-graphql/main')(ssb)
const profile = require('@ssb-graphql/profile')(ssb)

main.loadContext((err, context) => {
  ahauServer(
    {
      context,
      schemas: [main, profile],
      port: 18607
    }, (err, httpServer) => {
      // ready to query!

    })
  })
})
```

_See test/basic.test.js for a detailed example_


## API

### `ahauServer({ context, schemas, port, allowedOrigins }, cb)`

Calls back with a copy of the `httpServer`.

- `context` is an Object which will be accessible in graphql resolvers
- `schemas` is an Array of `{ typeDefs, resolvers }` to be combined
- `port` is the port that graphql server will listen on
- `allowedOrigins` (optional) is an array of additional origins which will be allowed to query
   - e.g. `[http://localhost:3000]`

You'll need to manually close this yourself when you're done with it (`httpServer.close()`)

You can also use with promises: 
```js
const server = await ahauServer({ context, schemas, port, allowedOrigins })
```

For development you can also configure the port to accept devServer requests from:
  - `DEV_SERVER_PORT=4000` enables CORS for http://localhost:4000
